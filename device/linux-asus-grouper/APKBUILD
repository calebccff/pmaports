# Maintainer: David Heidelberg <david@ixit.cz>
_flavor=asus-grouper
_commit="45a3363d0770af62325410c03535c51b73b0a38f"
_config="config-$_flavor.armv7"

pkgname=linux-asus-grouper
pkgver=5.6.0_rc1
pkgrel=0
arch="armv7"
pkgdesc="Nexus 7 grouper/tilapia (2012) mainline kernel"
url="https://postmarketos.org"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev
	     devicepkg-dev bison flex openssl-dev xz"
options="!strip !check !tracedeps"
source="
	$pkgname-$_commit.tar.gz::https://github.com/okias/linux/archive/$_commit.tar.gz
	$_config
"
license="GPL-2.0-only"

_carch="arm"

# Compiler: latest GCC from Alpine
_hostcc="${CC:-gcc}"
_hostcc="${_hostcc#${CROSS_COMPILE}}"

_ksrcdir="$srcdir/linux-$_commit"

prepare() {
	mkdir -p "$srcdir"/build
	cp -v "$srcdir"/$_config "$srcdir"/build/.config
	make -C "$_ksrcdir" O="$srcdir"/build ARCH="$_carch" HOSTCC="$_hostcc" \
		olddefconfig
}

build() {
	cd "$srcdir"/build
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-Alpine"
}

package() {
	install -Dm644 "$srcdir/build/arch/$_carch/boot/"*zImage \
		"$pkgdir/boot/vmlinuz-$_flavor"

	install -D "$srcdir/build/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	cd "$srcdir"/build

	local _install
	case "$CARCH" in
	aarch64*|arm*)	_install="modules_install dtbs_install" ;;
	*)		_install="modules_install" ;;
	esac

	make -j1 $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}
sha512sums="681bb00a75cdae677468ffd50463534fd5c03730c433f5243bfc245c50bd023d1b163dfd45ada53374b361ba9221e4cb4b76f26bddc3e04f0a0693266aa1ed6b  linux-asus-grouper-45a3363d0770af62325410c03535c51b73b0a38f.tar.gz
d4fb13567df269c147a4a5d427fdf85999b37898765f7260a0e6c47d717fde3b24e3f743d2bf47ea9c67a4855bbd6a7b3e0cd388c13e0bd896835ae6be05a1bf  config-asus-grouper.armv7"
